<?php

namespace Drupal\commerce_price_rule;

use Drupal\commerce_price_rule\Event\LoadAvailablePriceRulesEvent;
use Drupal\commerce_price_rule\Event\PriceRuleEvents;
use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce_store\Entity\StoreInterface;

use Drupal\Core\Entity\EntityTypeInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the price rule storage.
 */
class PriceRuleStorage extends CommerceContentEntityStorage implements
  PriceRuleStorageInterface {

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function loadAvailable(StoreInterface $store) {
    $today = gmdate('Y-m-d', $this->time->getRequestTime());
    $query = $this->getQuery()
      ->accessCheck(TRUE)
      ->condition('stores', [$store->id()], 'IN')
      ->condition('status', TRUE);

    // Start and end dates.
    $start_condition = $query->orConditionGroup()
      ->condition('start_date', NULL, 'IS NULL')
      ->condition('start_date', $today, '<=');
    $end_condition = $query->orConditionGroup()
      ->condition('end_date', NULL, 'IS NULL')
      ->condition('end_date', $today, '>=');
    $query->condition($start_condition)
      ->condition($end_condition);

    // Allow other modules to modify the query before we execute it.
    $event = new LoadAvailablePriceRulesEvent($query);
    $this->eventDispatcher->dispatch($event, PriceRuleEvents::LOAD_AVAILABLE);

    $result = $query->execute();
    if (empty($result)) {
      return [];
    }

    // Load and sort the price rules.
    $price_rules = $this->loadMultiple($result);
    uasort($price_rules, [$this->entityType->getClass(), 'sort']);

    return $price_rules;
  }

}
